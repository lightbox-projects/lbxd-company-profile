import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { StaticImage } from "gatsby-plugin-image"
import { navigate } from 'gatsby'

const LandingPage = ({ data, location }) => {
    const siteTitle = data.site.siteMetadata?.title || `Title`
    const project = data.project.frontmatter

    return (
        <Layout location={location} title={siteTitle}>
            <div className="d-flex flex-column position-relative bg-secondary" style={{ minHeight: '100vh', gap: '20px', overflowX: 'clip', overflowY: 'clip' }}>
                <div className='rounded-circle position-absolute' style={{ height: '1200px', aspectRatio: '1/1', border: '10px solid white', left: '-300px', top: '100px', opacity: .2, }}></div>
                <div className='rounded-circle position-absolute' style={{ height: '1500px', aspectRatio: '1/1', border: '3px solid white', right: '-300px', top: '-500px', opacity: .2, }}></div>

                <div className="mt-5 d-flex align-items-center " style={{ gap: '20px', marginLeft: '-20px', zIndex: '9999' }} onClick={() => navigate('/#portofolio')}>
                    <div className='rounded-circle d-flex' style={{ background: '#FFFF00', width: '90px', height: '90px', cursor: 'pointer' }} >
                        <i className='ti ti-corner-down-left-double ms-auto my-auto me-3' style={{ fontSize: '50px' }}></i>
                    </div>
                    <div className="display-4 fw-bolder text-white mb-0">
                        Back
                    </div>
                </div>

                <div className='custom-container' style={{ paddingBottom: '10em', zIndex: '9999' }}>
                    <div className='d-flex flex-column'>
                        <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                            <div className="carousel-indicators">
                                {
                                    project.gallery.map((item, key) => (
                                        key === 0 ?
                                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                                        :
                                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to={key} aria-label={`Slide ${key+1}`}></button>
                                    ))
                                }
                            </div>
                            <div className="carousel-inner">
                                {
                                    project.gallery.map((item, key) => (
                                        <div className={`carousel-item ${key === 0 ? 'active' : ''}`}>
                                            <div className="d-flex">
                                                <img src={item} alt="" style={{ maxHeight: '500px' }} className='m-auto' placeholder='none' />
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                <span className="carousel-control-prev-icon d-flex rounded-circle" style={{ background: '#FFFF00', width: '60px', height: '60px' }} aria-hidden="true">
                                    <i className="bx bx-chevron-left text-dark m-auto" style={{ fontSize: '50px' }}></i>
                                </span>
                                <span className="visually-hidden">Previous</span>
                            </button>
                            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                <span className="carousel-control-next-icon d-flex rounded-circle" style={{ background: '#FFFF00', width: '60px', height: '60px' }} aria-hidden="true">
                                    <i className="bx bx-chevron-right text-dark m-auto" style={{ fontSize: '50px' }}></i>
                                </span>
                                <span className="visually-hidden">Next</span>
                            </button>
                        </div>
                        <div className='text-white h3 text-center fw-bolder my-3'>{project.title}</div>
                    </div>

                    <div
                        dangerouslySetInnerHTML={{ __html: data.project.html }}
                        className="mt-5 text-white"
                        itemProp="articleBody"
                    />
                </div>

            </div>
        </Layout>
    )
}

export default LandingPage

export const Head = () => <Seo title="Project" />

export const pageQuery = graphql`
    query ($language: String!, $id: String!){
    site {
      siteMetadata {
        title
      }
    } 
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    } 
    project: markdownRemark(fields: {slug: {regex: "/^/projects/"}}, id: {eq: $id}) {
        frontmatter { 
          gallery
          size
          thumbnail
          title
          type
        }
        html
      }
  } 
` 
