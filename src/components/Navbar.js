import React from "react";
import { Link } from 'gatsby'
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';

const Navbar = ({ isAbsolute }) => {
    return (
        <nav className={`navbar navbar-expand-lg py-0 ${isMobile ? '' : 'px-5'} ${isAbsolute ? 'w-100 position-absolute' : ''}`} style={{ borderBottom: 'unset', top: 0, zIndex: 99, minHeight: '70px' }}>
            {/* <Trans>Hi people</Trans> */}
            <div className="container-fluid">
                {isAbsolute && !isMobile ? null :
                    <Link className="navbar-brand" to={'/'}>
                        {isMobile ?
                            <StaticImage src="../../static/assets/logo-horizontal.png" alt="" style={{ maxWidth: '130px' }} />
                            :
                            <StaticImage src="../../static/assets/logo.png" alt="" style={{ maxWidth: '70px' }} />
                        }
                    </Link>
                }
                <button className="navbar-toggler" type="button"
                    data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav"
                    aria-expanded="false" aria-label="Toggle navigation"
                    style={{ border: 'unset' }}
                >
                    {/* <span className="navbar-toggler-icon"></span> */}
                    <i className={`ti ti-menu-2 ${isAbsolute ? 'text-white' : ''}`}></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className={`navbar-nav ${isMobile ? 'justify-content-center' : ''} ${isAbsolute ? 'mx-auto' : 'ms-auto'}`} style={{ gap: isMobile ? '10px 20px' : '10px 40px', flexDirection: 'unset' }}>
                        <li className="nav-item">
                            <Link className="nav-link" aria-current="page" to={'/'}>Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" aria-current="page" to={'/listing'}>Listing</Link>
                        </li>  
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Navbar;