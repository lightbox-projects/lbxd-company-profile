import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import Drawer from 'react-modern-drawer'
import { navigate } from 'gatsby'


const Element = ({ projects }) => {
    const [isOpen, setIsOpen] = React.useState(false)
    const [filter, setFilter] = React.useState('Software Dev')
    const [contentFiltered, setContentFiltered] = React.useState([])

    const toggleDrawer = () => {
        setIsOpen((prevState) => !prevState)
    }

    const toggleFilter = (val) => {
        toggleDrawer()
        setFilter(val)
    }

    const openDetail = (slug) => {
        navigate('/project/' + slug)
    }

    React.useEffect(() => {
        setContentFiltered(projects.filter(item => (item.frontmatter.type === filter)))
    }, [filter])

    return (
        <div className="" style={{ overflowX: 'hidden' }}>
            <div className=" d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px' }}>
                {/* <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', top: '20px', left: '10px' }}>LBXD</div>
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', bottom: '45%', right: '10px' }}>LBXD</div> */}
                <div className='rounded-circle position-absolute' style={{ height: '500px', aspectRatio: '1/1', border: '4px solid white', right: '-300px', top: '20%' }}></div>
                <div className='rounded-circle position-absolute' style={{ height: '300px', aspectRatio: '1/1', border: '4px solid white', left: '-150px', top: '32%' }}></div>

                <div className="custom-container py-3 d-flex flex-wrap mt-4">
                    <div className="display-4 fw-bolder text-white" >
                        PORTFOLIO
                    </div>
                    <div className="h1 fw-bolder text-white ms-auto text-hover-tertiary" onClick={toggleDrawer} >
                        {filter}
                    </div>
                </div>
                <StaticImage src="../../../static/assets/ornament-1.png" alt="" class='position-absolute' style={{ top: '100px', right: '0px' }} />

                <div className="custom-container py-3 flex-fill d-flex flex-column" style={{ gap: '20px' }}>
                    <div className='row' style={{ gap: '30px 0' }}>
                        {
                            contentFiltered.map(item => (
                                <div className={`col-lg-${item.frontmatter.size}`} style={{ cursor: 'pointer' }} onClick={() => openDetail(item.fields.slug.replace('/projects/', ''))}>
                                    <div className='w-100 position-relative'>
                                        <img src={item.frontmatter.thumbnail} style={{ width: '100%', height: '250px', background: '#D9D9D9' }} />
                                        <div className='h4 text-white fw-bolder mb-0 position-absolute' style={{ bottom: '20px', left: '20px', textShadow: '0px 0px 5px grey' }}>
                                            {item.frontmatter.title}
                                        </div>
                                    </div>
                                    <StaticImage src="../../../static/assets/ornament-1.png" alt="" class='mt-4 ms-4' />
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>

            <Drawer
                open={isOpen}
                onClose={toggleDrawer}
                direction={'right'}
                className='bg-secondary w-100'
                style={{ borderLeft: '2px solid var(--tertiary)', maxWidth: '350px' }}
            >
                <div className='p-4 d-flex flex-column' style={{ gap: '20px 0' }}>
                    <div className='h3 fw-bolder text-nowrap text-white text-hover-tertiary' onClick={() => toggleFilter('Software Dev')}>Software Dev</div>
                    <div className='h3 fw-bolder text-nowrap text-white text-hover-tertiary' onClick={() => toggleFilter('Web Design')}>Web Design</div>
                    <div className='h3 fw-bolder text-nowrap text-white text-hover-tertiary' onClick={() => toggleFilter('Branding')}>Branding</div>
                    <div className='h3 fw-bolder text-nowrap text-white text-hover-tertiary' onClick={() => toggleFilter('Social Media')}>Social Media</div>
                    <div className='h3 fw-bolder text-nowrap text-white text-hover-tertiary' onClick={() => toggleFilter('Marketing Tools')}>Marketing Tools</div>
                </div>
            </Drawer>

        </div>
    );
}

export default Element