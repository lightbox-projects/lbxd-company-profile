import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import Drawer from 'react-modern-drawer'

const Element = () => {
    const [isOpenSoftware, setIsOpenSoftware] = React.useState(false)
    const [isOpenDCreative, setIsOpenDCreative] = React.useState(false)

    const toggleDrawer = (menu = null) => {
        if (!menu) {
            setIsOpenSoftware(false)
            setIsOpenDCreative(false)
        } else if (menu === 'software_house') {
            setIsOpenSoftware(true)
        } else if (menu === 'digital_creative') {
            setIsOpenDCreative(true)
        }
    }


    return (
        <div className="">
            <div className=" d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px' }}>
                {/* <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', top: '20px', left: '10px' }}>LBXD</div> */}
                <div className='rounded-circle position-absolute' style={{ height: '1500px', aspectRatio: '1/1', border: '10px solid white', left: '-600px', top: '-200px', opacity: .2, }}></div>

                <div className="custom-container py-3">
                    <div className="display-4 fw-bolder text-white mt-4" >
                        SERVICES
                    </div>
                </div>

                <div className="text-white custom-container py-3 flex-fill d-flex flex-column justify-content-center">
                    <div className='row align-items-center' style={{ gap: '10px 0' }}>
                        <div className='col-lg d-flex'>
                            <div className='m-auto position-relative text-hover-tertiary' onClick={() => toggleDrawer('software_house')}>
                                <div className='h1 fw-bolder'>Software House</div>
                                <StaticImage src="../../../static/assets/ornament-1.png" alt="..." className='position-absolute' style={{ transform: 'rotate(180deg)', right: '-2em' }} />
                            </div>
                        </div>
                        <div className='col-lg d-flex'>
                            <div className='position-relative m-auto text-hover-tertiary' style={{ height: '160px' }} onClick={() => toggleDrawer('digital_creative')}>
                                <div className='rounded-circle' style={{ height: '160px', width: '160px', border: '4px solid white' }}></div>
                                <div className='h-100 position-absolute d-flex flex-column justify-content-center' style={{ top: 0 }}>
                                    <div className='h1 fw-bolder text-nowrap mb-0 ms-2'>Digital Creative</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <Drawer
                open={isOpenSoftware}
                onClose={() => toggleDrawer()}
                direction={'left'}
                className='bg-secondary w-100'
                style={{ borderRight: '2px solid var(--tertiary)', maxWidth: '900px', zIndex: '999999' }}
            >
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', top: '20px', left: '10px' }}>LBXD</div>
                <StaticImage src="../../../static/assets/ornament-1.png" alt="..." className='position-absolute' style={{ right: '0', top: '30px' }} />

                <div className='d-flex align-items-center h-100 w-100'>
                    <div className='row custom-container align-items-center' style={{ gap: '30px 0' }}>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>IT Consultation</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Software Development</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Website Development</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Joint Research</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>IoT Research</div>
                        </div>
                    </div>
                </div>

                <StaticImage src="../../../static/assets/ornament-1.png" alt="..." className='position-absolute' style={{ transform: 'rotate(180deg)', bottom: '30px' }} />
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', bottom: '20px', right: '10px' }}>LBXD</div>
            </Drawer>
            <Drawer
                open={isOpenDCreative}
                onClose={() => toggleDrawer()}
                direction={'right'}
                className='bg-secondary w-100'
                style={{ borderLeft: '2px solid var(--tertiary)', maxWidth: '900px', zIndex: '999999'  }}
            >
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', top: '20px', left: '10px' }}>LBXD</div>
                <StaticImage src="../../../static/assets/ornament-1.png" alt="..." className='position-absolute' style={{ right: '0', top: '30px' }} />

                <div className='d-flex align-items-center h-100 w-100'>
                    <div className='row custom-container align-items-center position-relative' style={{ gap: '30px 0' }}>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Branding Consultation</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Social Media Analysist</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Brand Development</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Marketing Strategy Consultation</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>SEO & SEM Analysist</div>
                        </div>
                        <div className='col-lg-6'>
                            <div className='h3 mb-0 fw-bolder text-white text-hover-tertiary'>Animation / Illustration Production</div>
                        </div>
                    </div> 
                </div>

                <StaticImage src="../../../static/assets/ornament-1.png" alt="..." className='position-absolute' style={{ transform: 'rotate(180deg)', bottom: '30px' }} />
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', bottom: '20px', right: '10px' }}>LBXD</div>
            </Drawer>
        </div>
    );
}

export default Element