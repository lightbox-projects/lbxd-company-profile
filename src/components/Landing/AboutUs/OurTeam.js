import React from 'react'
import { StaticImage } from "gatsby-plugin-image"

const Element = (props) => {
    return (
        <div className="d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px', }}>

            <div className='navigation-circle' style={{ left: '-50px' }} onClick={() => props.handlePrev()}>
                <i className='ti ti-corner-down-left-double ms-auto my-auto me-3' style={{ fontSize: '50px' }}></i>
            </div>


            <div className="custom-container py-3">
                <div className="display-4 fw-bolder text-white mt-4" >
                    Our Team
                </div>
            </div>

            <div className='position-absolute w-100 h-100 d-flex' style={{ top: '0', left: '0px' }}>
                <StaticImage src="../../../../static/assets/Group 196.svg" alt="" class='m-auto' style={{ transform: 'scale(1.5)', mixBlendMode: 'luminosity' }} />
            </div>

            <div className='custom-container my-auto'>
                <div className='row' style={{ gap: '10px 0' }}>
                    <div className='col-lg-4'>
                        <div className='h1 fw-bolder text-white text-center'>Akhmad Dzakky R</div>
                    </div>
                    <div className='col-lg-4'>
                        <div className='h1 fw-bolder text-white text-center'>M Abdurrahman Al Jauzy</div>
                    </div>
                    <div className='col-lg-4'>
                        <div className='h1 fw-bolder text-white text-center'>Iqbal Fadhullah</div>
                    </div>
                </div>
            </div>

            <div className='custom-container'>
                <div className='h1 fw-bolder text-white '>The Mind Behid Everything</div>
                <div className='h3 fw-bolders text-white' style={{maxWidth: '1000px'}}>
                    Established in the year of 2024. to be the best partner and leading in transformative digital integration company in SEA
                </div>
            </div>

        </div>
    );
}

export default Element