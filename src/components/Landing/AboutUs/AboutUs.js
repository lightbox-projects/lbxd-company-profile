
import React from 'react'

const Element = (props) => {
    return (
        <div className="d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px', }}>
            <div className="custom-container py-3">
                <div className="display-4 fw-bolder text-white mt-4" >
                    ABOUT US
                </div>
            </div>
            
            <div className='navigation-circle' style={{ right: '-50px' }} onClick={() => props.handleNext()}>
                <i className='ti ti-corner-up-right-double ms-3 my-auto me-auto' style={{fontSize: '50px'}}></i>
            </div>

            <div className="custom-container py-3 flex-fill d-flex flex-column justify-content-start position-relative" style={{ gap: '50px', zIndex: '99999' }}>
                <div className='h2 fw-bolders text-white' style={{ maxWidth: '1200px' }}>
                    Established in the year of 2024. to be the best partner and leading in transformative digital integration company in SEA
                </div>
                <div className='h2 fw-bolders text-white' style={{ maxWidth: '1200px' }}>
                    LBX Digital aims to revolutionize the software development industry by offering thoroughly researched software and website development and combined with the creativity of digital creative.
                </div>
            </div>
        </div>
    );
}

export default Element
