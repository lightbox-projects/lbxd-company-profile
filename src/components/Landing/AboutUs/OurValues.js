import React from 'react'
import { StaticImage } from "gatsby-plugin-image"

const Element = (props) => {
    return (
        <div className="d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px', borderBottom: '4px solid white' }}>

            <div className='navigation-circle' style={{ left: '-50px' }} onClick={() => props.handlePrev()}>
                <i className='ti ti-corner-down-left-double ms-auto my-auto me-3' style={{fontSize: '50px'}}></i>
            </div>
            <div className='navigation-circle' style={{ right: '-50px' }} onClick={() => props.handleNext()}>
                <i className='ti ti-corner-up-right-double ms-3 my-auto me-auto' style={{fontSize: '50px'}}></i>
            </div>


            <div className="custom-container py-3">
                <div className="display-4 fw-bolder text-white mt-4" >
                    Our Values
                </div>
            </div>

            <div className='position-absolute w-100 h-100 d-flex' style={{ top: '0', left: '0px' }}>
                <StaticImage src="../../../../static/assets/Group 196.svg" alt="" class='m-auto' style={{transform: 'scale(1.5)', mixBlendMode: 'luminosity'}} />
            </div>


            <div className='row flex-fill' style={{ gap: '10px' }}>
                <div className='col-lg'>
                    <div className='h-100 d-flex flex-column'>
                        <div className='mx-auto mt-auto d-flex flex-column align-items-center'>
                            <div className='text-center fw-bolder h1 text-white mb-4'>
                                Integrity
                            </div>
                        </div>
                        <div className='mx-auto rounded-circle' style={{ background: 'white', height: '20px', aspectRatio: '1/1' }}></div>
                        <div className='mx-auto' style={{ background: 'white', height: '400px', width: '4px' }}></div>
                    </div>
                </div>
                <div className='col-lg'>
                    <div className='h-100 d-flex flex-column'>
                        <div className='mx-auto mt-auto d-flex flex-column align-items-center'>
                            <div className='text-center fw-bolder h1 text-white mb-4'>
                                Creativity
                            </div>
                        </div>
                        <div className='mx-auto rounded-circle' style={{ background: 'white', height: '20px', aspectRatio: '1/1' }}></div>
                        <div className='mx-auto' style={{ background: 'white', height: '300px', width: '4px' }}></div>
                    </div>
                </div>
                <div className='col-lg'>
                    <div className='h-100 d-flex flex-column'>
                        <div className='mx-auto mt-auto d-flex flex-column align-items-center'>
                            <div className='text-center fw-bolder h1 text-white mb-4'>
                                Innovation
                            </div>
                        </div>
                        <div className='mx-auto rounded-circle' style={{ background: 'white', height: '20px', aspectRatio: '1/1' }}></div>
                        <div className='mx-auto' style={{ background: 'white', height: '200px', width: '4px' }}></div>
                    </div>
                </div>
                <div className='col-lg'>
                    <div className='h-100 d-flex flex-column'>
                        <div className='mx-auto mt-auto d-flex flex-column align-items-center'>
                            <div className='text-center fw-bolder h1 text-white mb-4'>
                                Intelligent
                            </div>
                        </div>
                        <div className='mx-auto rounded-circle' style={{ background: 'white', height: '20px', aspectRatio: '1/1' }}></div>
                        <div className='mx-auto' style={{ background: 'white', height: '400px', width: '4px' }}></div>
                    </div>
                </div>
                <div className='col-lg'>
                    <div className='h-100 d-flex flex-column'>
                        <div className='mx-auto mt-auto d-flex flex-column align-items-center'>
                            <div className='text-center fw-bolder h1 text-white mb-4'>
                                Professional
                            </div>
                        </div>
                        <div className='mx-auto rounded-circle' style={{ background: 'white', height: '20px', aspectRatio: '1/1' }}></div>
                        <div className='mx-auto' style={{ background: 'white', height: '300px', width: '4px' }}></div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default Element