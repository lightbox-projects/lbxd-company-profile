import React from 'react'

import AboutUs from './AboutUs/AboutUs'
import OurValues from './AboutUs/OurValues'
import OurTeam from './AboutUs/OurTeam'

const Element = () => {

    const [activeIdx, setActiveIdx] = React.useState(0)

    function handlePrev() {
        setActiveIdx(activeIdx - 1)
    }
    
    function handleNext() {
        setActiveIdx(activeIdx + 1)
    }

    return (
        <div className="position-relative" style={{overflowX: 'clip',  }}>
            <div className='rounded-circle position-absolute' style={{ height: '1800px', aspectRatio: '1/1', border: '4px solid white', right: '-250px', top: '0px', opacity: .2, }}></div>

            {activeIdx === 0 && <AboutUs handleNext={handleNext} handlePrev={handlePrev}  />}
            {activeIdx === 1 && <OurValues handleNext={handleNext} handlePrev={handlePrev}  />}
            {activeIdx === 2 && <OurTeam handleNext={handleNext} handlePrev={handlePrev}  />}

        </div>
    );
}

export default Element