import React from 'react'
import { Helmet } from "react-helmet";
import anime from 'animejs';

import { useLottie } from "lottie-react";
import LBXDJSON from "../../static/assets/LBXD.json";

import {
  RecoilRoot,
} from 'recoil'

const RootWrapper = ({ children }) => {

  const [justOpened, setJustOpened] = React.useState(false)

  const options = {
    animationData: LBXDJSON,
    loop: false
  };

  const { View } = useLottie(options);

  React.useEffect(() => {
    setTimeout(() => {
      anime({
        targets: '#backdrop-loading',
        keyframes: [
          { opacity: 1 },
          { opacity: 0 },
        ],
        easing: 'easeInOutSine',
        duration: 1000
      });
      setTimeout(() => {
        setJustOpened(false)
      }, 500)
    }, 4000)
  }, [])

  return (
    <div>
      <Helmet>
        {/* <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" 
                rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossOrigin="anonymous"/> */}
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossOrigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/tabler-icons.min.css" />
        <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet' />

        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet" />
      </Helmet>

      <RecoilRoot>
        <div className='position-relative' style={{ height: justOpened ? '100vh' : 'unset', overflowY: justOpened ? 'hidden' : 'unset' }}>
          {justOpened &&
            <div className={`position-fixed vh-100 w-100 bg-secondary d-flex`} id="backdrop-loading" style={{ top: '0', left: '0', zIndex: 9999999 }}>
              <div className='w-100'>{View}</div>
            </div>
          }
          {children}
        </div>
      </RecoilRoot>

    </div>
  );
}

export default RootWrapper