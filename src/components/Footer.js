import React  from "react";
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';

const Footer = ({ children }) => {
    const [state, setState] = React.useState({
        name: '',
        email: '',
        body: '',
    });

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
    };

    function handleContact() {
        let subject = `MegacityBangkok, Contact from ${state.name}`
        window.open(`mailto:thawdarkhant@gmail.com?subject=${subject}&body=${state.body}`)
    }

    return (
        <div className="">
            <div className="bg-secondary d-flex flex-column position-relative" style={{ minHeight: '100vh', gap: '20px' }}>
                <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', top: '20px', left: '10px' }}>LBXD</div>

                <div className="custom-container py-3">
                    <div className="display-3 fw-bolder text-white mt-4" >
                        CONTACT US
                    </div>
                </div>

                <div className="custom-container py-3 flex-fill d-flex flex-column" style={{ gap: '20px' }}>

                    <div className="row my-auto" style={{ gap: '10px 0' }}>
                        <div className="col-lg-3">
                            <div className={`d-flex align-items-center mb-5 mt-5 ${isMobile ? 'justify-content-center' : ''}`} style={{ gap: '10px' }}>
                                <StaticImage src="../../static/assets/logo.png" alt="Logo Footer" style={{}} />
                            </div>
                            <div className="d-flex flex-column" style={{ gap: '30px' }}>
                                <div className="h4 fw-bolder text-white mb-0" >
                                    8 Jln Kilang Timor, #03-14 Kewalram Hse, Singapore 159305
                                </div>
                                <div className="h4 fw-bolder text-white mb-0" >
                                    (+65) 12923128
                                </div>
                                <div className="h4 fw-bolder text-white mb-0" >
                                    LBXDIGITAL@GMAIL.COM
                                </div>
                            </div>
                            <div style={{ fontSize: '15px' }} className="fw-bolder text-white mb-2 mt-4" >
                                Find Us On
                            </div>
                            <div className="d-flex text-white fw-bolder" style={{ gap: '10px' }}>
                                <i className="ti ti-brand-facebook" style={{ fontSize: '30px' }}></i>
                                <i className="ti ti-brand-x" style={{ fontSize: '30px' }}></i>
                                <i className="ti ti-brand-instagram" style={{ fontSize: '30px' }}></i>
                            </div>
                        </div>
                        <div className="col-lg">
                            <form id="form-footer">

                                <div className="h3 fw-bolder text-white mb-3" >
                                    Lets Grow With Us!
                                </div>

                                <div className="d-flex flex-column" style={{ gap: '20px 0' }}>
                                    <div className="">
                                        <input className="form-control-outline" value={state.name} onChange={event => handleChange(event, 'name')} placeholder={"Name"} id="footer_name" required="" />
                                    </div>
                                    <div className="">
                                        <input className="form-control-outline" value={state.email} onChange={event => handleChange(event, 'email')} placeholder={"Email"} id="footer_email" required="" />
                                    </div>
                                    <div className="">
                                        <textarea className="form-control-outline" value={state.body} onChange={event => handleChange(event, 'body')}
                                            id="footer_desc" required=""
                                            placeholder={"Ask your question and send it right away to our inbox"} rows="10"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <div className="w-100 mt-auto p-3 text-center" style={{ gap: '10px' }}>
                    <div className="h6 mb-0 text-white" style={{ fontSize: '14px' }}>
                        LBX DIGITAL {new Date().getFullYear()}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer