import * as React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { StaticImage } from "gatsby-plugin-image"

// import Lottie from '../components/Landing/Lottie'
import AboutUs from '../components/Landing/AboutUs'
import Portofolio from '../components/Landing/Portfolio'
import Services from '../components/Landing/Services'

const LandingPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  const projects = data.projects.nodes

  return (
    <Layout location={location} title={siteTitle}>

      <div className="position-relative bg-secondary">
        <div className="position-fixed w-100" style={{ minHeight: '100vh', opacity: 1 }}>
          {/* <Lottie /> */}
        </div>


        <div>
          <div className="w-100 d-flex flex-column" style={{ minHeight: '100vh', overflowX: 'hidden' }}>
            <div className="custom-container position-relative flex-fill">
              {/* <div className="text-white fw-bolder h3 mb-0 position-absolute" style={{ writingMode: 'vertical-rl', textOrientation: 'upright', bottom: '180px', right: '10px' }}>LBXD</div> */}
              <div className='rounded-circle position-absolute' style={{ height: '500px', aspectRatio: '1/1', border: '4px solid white', right: '-300px', bottom: '0px' }}></div>


              <Link to="/#about-us" className="text-white text-hover-tertiary position-absolute d-flex align-items-center" style={{ bottom: '55%', zIndex: 9, left: '200px', textDecoration: 'unset' }}>
                <div className="d-flex flex-column">
                  <div className='h4 mb-0'>Find Out More</div>
                  <div className='h2 mb-0 fw-bolder'>ABOUT US</div>
                </div>
                <i className="ti ti-player-play-filled ms-3" style={{ fontSize: '50px' }}></i>
              </Link>

              <StaticImage src="../../static/assets/ornament-2.png" alt="..." className='position-absolute' style={{ transform: '180deg', left: '0px', bottom: '40%' }} />

              <div className="text-white fw-bolder h3 my-5">creative crews in transformative digital experiences both in technological advancement and digital creative strategy.</div>
            </div>
          </div>

          <section id="about-us">
            <AboutUs />
          </section>
          <Services />
          <section id="portofolio">
            <Portofolio projects={projects} />
          </section>
        </div>
      </div>

    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Landing Page" />

export const pageQuery = graphql`
  query ($language: String!){
    site {
      siteMetadata {
        title
      }
    } 
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    projects: allMarkdownRemark {
      nodes {
        fields {
          slug
        }
        frontmatter {
          date(formatString: "DD MMMM YYYY")
          title
          size
          thumbnail
          gallery
          type
          description
        }
      }
    }
  } 
` 
