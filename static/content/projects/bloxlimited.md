---
title: Bloxlimited
date: 2024-04-22T14:09:55.579Z
thumbnail: /img/image-536.png
gallery:
  - /img/image-536.png
  - /img/image-537.png
  - /img/image-537-1-.png
type: Software Dev
size: "4"
---
Multi Service Lighting Tools Platform\
The new way of providing valuable solutions from design, products search, supply chain, installation services and lighting subscription.

## **S﻿cope of Works**

Landing Page\
Back-End System\
Dashboard\
Web Application

## **S﻿tack Used**

L﻿aravel, Javascript, Postgresql, Bootstrap