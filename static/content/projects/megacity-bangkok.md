---
title: Megacity Bangkok
date: 2024-04-25T09:06:48.112Z
thumbnail: /img/megacity-1.png
gallery:
  - /img/megacity-1.png
  - /img/megacity-2.png
  - /img/megacity-3.png
type: Software Dev
size: "4"
---
M﻿egacity is more than just a property listing company. We're your trusted companion on the exciting journey of finding your perfect home. Whether you prefer a sleek urban apartment or a serene suburban retreat. Your Trusted Property Companion.

## **S﻿cope of Works**

Landing Page\
P﻿roperty Detail Page\
C﻿ompany Profile Page\
Netlify Content Management System

## **S﻿tack Used**

Gatsby Js, React Js, Bootstrap, Netlify