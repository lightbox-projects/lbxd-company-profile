---
title: Qompair
date: 2024-04-22T14:19:49.313Z
thumbnail: /img/qompair-1.png
gallery:
  - /img/qompair-1.png
  - /img/qompair-2.png
  - /img/qompair-3.png
type: Software Dev
size: "4"
---
Qompair is a platform to collaborate between project owner and bidder. With a wide variation of functions such as product list, tender system, bid review submission and many more, we aim to be the easiest method as a 3rd party to connecting between a project owner and bidder thus creating a deal and cooperation.

## **S﻿cope of Works**

Landing Page\
B﻿idding System\
P﻿roject Host System\
Dashboard Monitoring\
B﻿ackend System\
P﻿ayment Gateway

## **S﻿tack Used**

L﻿aravel, Javascript, Postgresql, Bootstrap