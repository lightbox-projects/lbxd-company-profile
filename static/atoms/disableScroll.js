import {
    atom,
} from 'recoil';

const disableScroll = atom({
    key: 'disableScroll',
    default: false,
});

export default disableScroll;